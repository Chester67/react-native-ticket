export interface Input {
  id: number;
  value: String;
}
