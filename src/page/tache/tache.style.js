import {StyleSheet} from 'react-native';
export default StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    marginTop: 20,
  },
  inputArea: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  listArea: {
    paddingTop: 20,
    alignItems: 'center',
    marginVertical: 15,
  },
});
