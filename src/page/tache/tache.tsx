import * as React from 'react';
import {Component} from 'react';
import {Platform, View} from 'react-native';
import styles from './tache.style';
import AddList from '../../component/list/AddList';
import ItemsList from '../../component/list/ItemsList';
import {Input} from '../../model/input.model';
import ErrorList from '../../component/error/ErrorList';

interface IProps {}
interface IState {
  list: Input[];
  listInput: String;
  error: boolean;
}

class Tache extends Component<IProps, IState> {
  /*
  const [listInput, setListInput] = useState('')
  const [list, setList] = useState([]);
  const [error, setError] = useState(false);
  */
  constructor(props: IProps) {
    super(props);

    this.state = {
      list: [],
      listInput: '',
      error: false,
    };
  }

  private static TEXT_INSTRUCTIONS = Platform.select({
    ios: `Press Cmd+R to reload,
Cmd+D or shake for de  v menu`,

    android: `Double tap R on your keyboard to reload,
Shake or press menu button for dev menu`,
  });

  handleInput = (text: String) => {
    this.setState({listInput: text});
  };

  handleSetList = () => {
    console.log('pass this out 1');
    if (!this.state.listInput) {
      this.setState({error: true});
      return;
    }

    console.log('pass this out 2');
    const newInput: Input = {
      id: Math.random() * 1000,
      value: this.state.listInput,
    };

    var tempList: Input[] = this.state.list;
    tempList.push(newInput);
    this.setState({list: tempList});
    this.setState({listInput: ''});
    this.setState({error: false});

    console.log('pass this out 3 =>' + JSON.stringify(this.state.list));
  };

  handleDelete = (id: number) => {
    console.log('click delete' + id);
    this.setState({list: this.state.list.filter((item) => item.id !== id)});
  };

  public render() {
    const {listInput, list, error} = this.state;
    return (
      <View style={styles.container}>
        <View style={styles.inputArea}>
          <AddList
            onPressAdd={this.handleSetList}
            value={listInput}
            onChange={this.handleInput}
          />
        </View>
        <ItemsList data={list} onDelete={this.handleDelete} />
        <ErrorList visible={error}>Please insert a valid text</ErrorList>
      </View>
    );
  }
}

export default Tache;
