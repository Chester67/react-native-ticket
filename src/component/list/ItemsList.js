import React from 'react';
import ListItem from './ListItem';
import {View, FlatList, StyleSheet, Dimensions} from 'react-native';

const {Height} = Dimensions.get('window');

export default function ItemsList({data, onDelete}) {
  const renderFunction = ({item}) => (
    <ListItem style={styles.listItemStyles} item={item} onDelete={onDelete} />
  );

  return (
    <View style={styles.container}>
      <FlatList
        style={styles.flat}
        data={data}
        renderItem={renderFunction}
        keyExtractor={(item) => String(item.id)}
        ontentContainerStyle={{paddingBottom: 20}}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100%',
    alignItems: 'center',
    height: 400,
    marginTop: 80,
  },
  listItemStyles: {
    marginVertical: 3,
  },
  flat: {
    width: '80%',
  },
});
